package com.mcc.wpnews.models
import com.google.gson.annotations.SerializedName


/**
 * Created by Leo Perez Ortíz on 31/3/18.
 */



data class Videos(
		@SerializedName("items") val items: ArrayList<Item>
)

data class Item(
		@SerializedName("id") val id: String,
		@SerializedName("title") val title: String,
		@SerializedName("video") val video: String,
		@SerializedName("image") val image: String
)
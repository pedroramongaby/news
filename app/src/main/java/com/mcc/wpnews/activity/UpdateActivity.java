package com.mcc.wpnews.activity;

import android.app.DownloadManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.SpanUtils;
import com.mcc.wpnews.Manifest;
import com.mcc.wpnews.R;
import com.mcc.wpnews.api.http.ApiUtils;
import com.mcc.wpnews.utility.PermissionUtils;
import com.mcc.wpnews.utility.UtilsWhatsApp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity {

    private String new_version;
    private String current_version;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ApiUtils.getApiInterface().getWhatSite("http://www.whatsapp.com/android/")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                String html = response.body().string();
                                Document doc = Jsoup.parse(html);
                                Element link = doc.selectFirst("a#action-button");
                                if(link== null)
                                    return;
                                url= link.attr("href");
                                Element version = doc.selectFirst("[class=hint version--hint]");
                                new_version= version.text().split(" ")[1];
                                SpanUtils span = new SpanUtils().append("WhatsApp Avalible ").append(version.text())
                                        .setBold();
                                ((TextView) findViewById(R.id.avalible)).setText(span.create());
                                findViewById(R.id.progressBar).setVisibility(View.GONE);
                                findViewById(R.id.avalible).setVisibility(View.VISIBLE);
                                if(current_version!=null && !current_version.equals(new_version)){
                                    findViewById(R.id.avalible3).setVisibility(View.VISIBLE);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        if (UtilsWhatsApp.isWhatsAppInstalled(this)) {
            current_version=UtilsWhatsApp.getInstalledWhatsAppVersion(this);
            SpanUtils span = new SpanUtils().append("Installed WhatsApp Version: ")
                    .append(current_version)
                    .setBold();
            ((TextView) findViewById(R.id.avalible2))
                    .setText(span.create());
        } else {
            ((TextView) findViewById(R.id.avalible2)).setText("WhatsApp not Installed");
        }
        findViewById(R.id.avalible3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(PermissionUtils.isPermissionGranted(UpdateActivity.this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},23)){
                    donwLoad();
                }else{

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    private void donwLoad() {
        // Create request for android download manager
        DownloadManager  downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

        //Setting title of request
        request.setTitle("WhatsApp Download");

        //Setting description of request
        request.setDescription("Whatssapp "+new_version);

        //Set the local destination for the downloaded file to a path within the application's external files directory
        request.setDestinationInExternalFilesDir(UpdateActivity.this, Environment.DIRECTORY_DOWNLOADS,"AndroidTutorialPoint.jpg");

        //Enqueue download and save into referenceId
        downloadManager.enqueue(request);
        Toast.makeText(this,"WhatsApp is downloading...",Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(PermissionUtils.isPermissionResultGranted(grantResults))
            findViewById(R.id.avalible3).performClick();
    }
}

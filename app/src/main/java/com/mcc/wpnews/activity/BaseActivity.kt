package com.mcc.wpnews.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import com.google.android.gms.appinvite.AppInviteInvitation
import com.mcc.wpnews.R
import com.mcc.wpnews.data.constant.AppConstant
import com.mcc.wpnews.utility.ActivityUtils
import com.mcc.wpnews.utility.AppUtils
import com.mcc.wpnews.utility.PermissionUtils
import com.mcc.wpnews.utility.ExtraAdsManager
import org.jetbrains.anko.email
import org.jetbrains.anko.intentFor


/**
 * Created by Ashiq on 10/10/16.
 */
open class BaseActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var mContext: Context? = null
    private var mActivity: Activity? = null

    // global toolbar
    /**
     * Access toolbar
     */
    var toolBar: Toolbar? = null
        private set
    var drawerLayout: DrawerLayout? = null
        private set
    var navigationView: NavigationView? = null
        private set

    private var loadingView: LinearLayout? = null
    private var noDataView: LinearLayout? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mActivity = this@BaseActivity
        mContext = mActivity!!.applicationContext

        // uncomment this line to disable ad
        // AdUtils.getInstance(mContext).disableBannerAd();
        // AdUtils.getInstance(mContext).disableInterstitialAd();

    }

    fun initToolbar() {
        toolBar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolBar)
    }

    fun initDrawer(enable: Boolean) {

        // Initialize drawer view
        drawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout

        if (enable) {
            val toggle = object : ActionBarDrawerToggle(this, drawerLayout, toolBar, R.string.openDrawer, R.string.closeDrawer) {
                override fun onDrawerClosed(view: View) {
                    super.onDrawerClosed(view)

                }

                override fun onDrawerOpened(drawerView: View) {
                    super.onDrawerOpened(drawerView)

                }
            }


            drawerLayout!!.setDrawerListener(toggle)
            toggle.syncState()

            navigationView = findViewById<View>(R.id.navigationView) as NavigationView
            navigationView!!.itemIconTintList = null
            navigationView!!.setNavigationItemSelectedListener(this)
        } else {
            drawerLayout!!.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }


    fun setToolbarTitle(title: String) {
        if (supportActionBar != null) {
            supportActionBar!!.title = title
        }
    }

    fun enableBackButton() {
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
    }

    fun initLoader() {
        loadingView = findViewById<View>(R.id.loadingView) as LinearLayout
        noDataView = findViewById<View>(R.id.noDataView) as LinearLayout
    }


    fun showLoader() {
        if (loadingView != null) {
            loadingView!!.visibility = View.VISIBLE
        }

        if (noDataView != null) {
            noDataView!!.visibility = View.GONE
        }
    }

    fun hideLoader() {
        if (loadingView != null) {
            loadingView!!.visibility = View.GONE
        }
        if (noDataView != null) {
            noDataView!!.visibility = View.GONE
        }
    }

    fun showEmptyView() {
        if (loadingView != null) {
            loadingView!!.visibility = View.GONE
        }
        if (noDataView != null) {
            noDataView!!.visibility = View.VISIBLE
        }
    }

    fun showAdThenActivity(activity: Class<*>) {
        ExtraAdsManager.showInterstitial ({
            ActivityUtils.getInstance().invokeActivity(mActivity, activity, false)
            null
        }, true)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (PermissionUtils.isPermissionResultGranted(grantResults)) {
            if (requestCode == PermissionUtils.REQUEST_CALL) {
                AppUtils.makePhoneCall(mActivity, AppConstant.CALL_NUMBER)
            }
        } else {
            AppUtils.showToast(mActivity, getString(R.string.permission_not_granted))
        }

    }

    override fun onResume() {
        super.onResume()
        ExtraAdsManager.showInterstitial ({ null }, true)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_home) {
            ExtraAdsManager.showInterstitial( {
                ActivityUtils.getInstance().invokeActivity(mActivity, MainActivity::class.java, true)
                finish()
                null
            }, true)
        } else if (id == R.id.action_menus) {
            ActivityUtils.getInstance().invokeActivity(mActivity, MenuListActivity::class.java, false)

        } else if (id == R.id.action_categories) {
            ActivityUtils.getInstance().invokeActivity(mActivity, CategoryListActivity::class.java, false)
        } else if (id == R.id.action_favourite) {
            ExtraAdsManager.showInterstitial ({
                ActivityUtils.getInstance().invokeActivity(mActivity, FavouriteListActivity::class.java, false)
                null
            }, true)
        } else if (id == R.id.update) {
            ActivityUtils.getInstance().invokeActivity(mActivity, UpdateActivity::class.java, false)
        } else if (id == R.id.videos) {
            ExtraAdsManager.showInterstitial ({
                ActivityUtils.getInstance().invokeActivity(mActivity, VideoActivity::class.java, false)
                null
            }, true)
        } else if (id == R.id.action_youtube) {
            AppUtils.youtubeLink(mActivity)
        } else if (id == R.id.action_facebook) {
            AppUtils.faceBookLink(mActivity)
        } else if (id == R.id.action_twitter) {
            AppUtils.twitterLink(mActivity)
        } else if (id == R.id.action_google_plus) {
            AppUtils.googlePlusLink(mActivity)
        } else if (id == R.id.action_call) {
            AppUtils.makePhoneCall(mActivity, AppConstant.CALL_NUMBER)
        } else if (id == R.id.action_message) {
            AppUtils.sendSMS(mActivity, AppConstant.SMS_NUMBER, AppConstant.SMS_TEXT)
        } else if (id == R.id.action_messenger) {
            AppUtils.invokeMessengerBot(mActivity)
        } else if (id == R.id.action_email) {
            email(AppConstant.EMAIL_ADDRESS,AppConstant.EMAIL_SUBJECT,AppConstant.EMAIL_BODY)
        } else if (id == R.id.action_share) {
            ExtraAdsManager.showInterstitial ({
                val intent = AppInviteInvitation.IntentBuilder("Share")
                        .setMessage(getString(R.string.share_text))
                        .setDeepLink(Uri.parse("https://kt8t6.app.goo.gl/isR1"))
//                        .setCustomImage(Uri.parse("https://lh3.googleusercontent.com/LSJ2Pj7GpdNnz4UTFH-CB-N6ik4be5o3ETZj5qd-6IcWaOWjiGk-DoYHqfcWQaO-Gqln=w1440-h620-rw"))
                        .setCallToActionText("GET")
                        .build()
                startActivityForResult(intent,23)
//                share("${getString(R.string.share_text)} https://play.google.com/store/apps/details?id=${packageName}")
                null
            }, true)
        } else if (id == R.id.action_rate_app) {
            ExtraAdsManager.showInterstitial ({
                AppUtils.rateThisApp(mActivity) // this feature will only work after publish the app
                null
            }, true)
        } else if (id == R.id.action_settings) {
            ExtraAdsManager.showInterstitial ({
                ActivityUtils.getInstance().invokeActivity(mActivity, SettingsActivity::class.java, false)
                null
            }, true)
        } else if (id == R.id.terms_conditions) {
            ActivityUtils.getInstance().invokeCustomPostAndLink(mActivity, CustomLinkAndPageActivity::class.java, resources.getString(R.string.terms), resources.getString(R.string.terms_url), false)
        } else if (id == R.id.privacy_policy) {
            ActivityUtils.getInstance().invokeCustomPostAndLink(mActivity, CustomLinkAndPageActivity::class.java, resources.getString(R.string.privacy), resources.getString(R.string.privacy_url), false)
        } else if (id == R.id.faq) {
            ActivityUtils.getInstance().invokeCustomPostAndLink(mActivity, CustomLinkAndPageActivity::class.java, resources.getString(R.string.faq), resources.getString(R.string.faq_url), false)
        }
        if (drawerLayout != null && drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        }

        return true

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(this@BaseActivity.toString(), "onActivityResult: requestCode=$requestCode, resultCode=$resultCode")

        if (requestCode == 23) {
            if (resultCode == Activity.RESULT_OK) {
                // Get the invitation IDs of all sent messages
                val ids = AppInviteInvitation.getInvitationIds(resultCode, data)

            } else {
                // Sending failed or it was canceled, show failure message to the user
                // ...
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}
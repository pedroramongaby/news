package com.mcc.wpnews.adapters

import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.mcc.wpnews.R
import com.mcc.wpnews.models.Item
import kotlinx.android.synthetic.main.video_item.view.*
import com.mcc.wpnews.utility.ExtraAdsManager
import leoperez.com.myapplication.extention.GlideApp
import leoperez.com.myapplication.extention.inflate

/**
 * Created by Leo Perez Ortíz on 31/3/18.
 */
class VideoAdapter(val items: ArrayList<Item>): RecyclerView.Adapter<VideoAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
    }

    override fun getItemCount()= items.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= VideoAdapter.ViewHolder(parent.inflate(R.layout.video_item))


    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(item: Item) {
            itemView.apply {
                GlideApp.with(context).load(item.image).placeholder(R.drawable.video_place).into(post_img)
                title_text.text= item.title
                setOnClickListener({
                    ExtraAdsManager.showInterstitial({
                        context.startActivity(Intent(Intent.ACTION_VIEW,Uri.parse(item.video)))
                    }, FirebaseRemoteConfig.getInstance().getBoolean("ads"))
                })
            }
        }

    }
}
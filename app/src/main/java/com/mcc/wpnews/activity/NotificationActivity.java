package com.mcc.wpnews.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdView;
import com.mcc.wpnews.R;
import com.mcc.wpnews.adapters.NotificationAdapter;
import com.mcc.wpnews.data.constant.AppConstant;
import com.mcc.wpnews.data.sqlite.NotificationDbController;
import com.mcc.wpnews.listeners.ListItemClickListener;
import com.mcc.wpnews.models.NotificationModel;
import com.mcc.wpnews.utility.AdUtils;

import java.util.ArrayList;

public class NotificationActivity extends BaseActivity {

    private Context mContext;
    private Activity mActivity;

    private RecyclerView recyclerView;
    private NotificationAdapter mAdapter;
    private ArrayList<NotificationModel> arrayList;

    private NotificationDbController notificationDbController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = NotificationActivity.this;
        mContext = mActivity.getApplicationContext();

        initVars();
        initialView();
        initFunctionality();
        initialListener();
    }

    private void initVars() {
        arrayList = new ArrayList<>();
    }

    private void initialView() {
        setContentView(R.layout.activity_notification);

        //productList
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new NotificationAdapter(mActivity, arrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(mAdapter);

        initLoader();

        initToolbar();
        setToolbarTitle(getString(R.string.notifications));
        enableBackButton();
    }

    private void initFunctionality() {
        notificationDbController = new NotificationDbController(mContext);

        showLoader();
        arrayList.addAll(notificationDbController.getAllData());

        hideLoader();

        if (arrayList.size() == 0) {
            showEmptyView();
        } else {
            mAdapter.notifyDataSetChanged();
        }
    }

    private void initialListener() {

        mAdapter.setItemClickListener(new ListItemClickListener() {
            @Override
            public void onItemClick(int position, View view) {
                notificationDbController.updateStatus(arrayList.get(position).getId(), true);

                Intent intent = new Intent(mContext, NotificationDetailsActivity.class);
                intent.putExtra(AppConstant.BUNDLE_KEY_TITLE, arrayList.get(position).getTitle());
                intent.putExtra(AppConstant.BUNDLE_KEY_MESSAGE, arrayList.get(position).getMessage());
                intent.putExtra(AppConstant.BUNDLE_KEY_POST_ID, arrayList.get(position).getPostId());
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AdUtils.getInstance(mContext).showBannerAd((AdView) findViewById(R.id.adView));
    }
}

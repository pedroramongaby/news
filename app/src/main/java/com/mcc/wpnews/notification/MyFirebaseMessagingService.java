package com.mcc.wpnews.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mcc.wpnews.R;
import com.mcc.wpnews.activity.MainActivity;
import com.mcc.wpnews.activity.PostDetailsActivity;
import com.mcc.wpnews.data.constant.AppConstant;
import com.mcc.wpnews.data.preference.AppPreference;
import com.mcc.wpnews.data.sqlite.NotificationDbController;

import java.util.Map;

/**
 * Created by Ashiq on 4/19/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("==", "remote message size: " + remoteMessage.getData().size());

        if (remoteMessage.getData().size() > 0) {
            Map<String, String> params = remoteMessage.getData();

            if (AppPreference.getInstance(MyFirebaseMessagingService.this).isNotificationOn()) {

                Log.d("TAG", "<----- notifiaction is on --------->");
                sendNotification(params.get("title"), params.get("message"), params.get("newsId"));
                broadcastNewNotification();
            } else {
                Log.d("TAG", "<------ notifiaction is off ----------->");
            }
        }
    }

    private void sendNotification(String title, String messageBody, String postId) {

        // insert data into database
        NotificationDbController notificationDbController = new NotificationDbController(MyFirebaseMessagingService.this);
        notificationDbController.insertData(title, messageBody, postId);

        int id = -1;
        try {
            id = Integer.parseInt(postId);
        } catch (Exception e) {
        }

        Intent intent;
        if (id != -1) {
            intent = new Intent(this, PostDetailsActivity.class);
            intent.putExtra(AppConstant.BUNDLE_KEY_TITLE, title);
            intent.putExtra(AppConstant.BUNDLE_KEY_POST_ID, id);
        } else {
            intent = new Intent(this, MainActivity.class);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000, 1000})
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void broadcastNewNotification() {
        Intent intent = new Intent(AppConstant.NEW_NOTI);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}

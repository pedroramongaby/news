package com.mcc.wpnews.utility

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.util.Log
import android.view.ContextMenu
import android.view.View
import com.google.android.gms.ads.*
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import org.joda.time.DateTime
import org.joda.time.Seconds
import java.util.concurrent.atomic.AtomicReference
import com.codemybrainsout.ratingdialog.RatingDialog
import leoperez.com.myapplication.extention.launchUrl


/**
 * Created by Leo Perez Ortíz on 29/3/18.
 */
class ExtraAdsManager {

    companion object {
        //app reference
        val app = AtomicReference<Application>()
        //keys reference
        val interistial_admob = AtomicReference<String>()
        val interistial_facebook = AtomicReference<String>()
        //interistial admob
        lateinit var interstitialAd: InterstitialAd
        lateinit var request: AdRequest
        //action to perform on adclosed
        lateinit var onClose: () -> Unit
        //ads show counters
        var facebook_count = 1
        var admob_count = 1

        fun initManager(app: Application, interistial_admob: String, interistial_facebook: String) {
            this@Companion.app.set(app)
            this@Companion.interistial_admob.set(interistial_admob)
            this@Companion.interistial_facebook.set(interistial_facebook)
            MobileAds.initialize(app, "ca-app-pub-1502760063615827~8828221526")
            //load admob
            interstitialAd = InterstitialAd(this@Companion.app.get());
            interstitialAd.adUnitId = interistial_admob
            request = AdRequest.Builder().build()
            interstitialAd.adListener = object : AdListener() {
                override fun onAdClosed() {
                    Log.d("Ads", "Ads close")
                    interstitialAd.loadAd(request)
                    onClose()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    onClose = {}
                    Log.d("Ads", "Ads left application")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Log.d("admob", "Admob Interistial loaded")
                }
            }
            interstitialAd.loadAd(request)
        }

        fun showInterstitial(action_close: () -> Unit, config: Boolean) {
            onClose = action_close
            if (!FirebaseRemoteConfig.getInstance().getBoolean("ads")) {
                onClose()
                return
            }
            var pf = app.get().getSharedPreferences("PF", MODE_PRIVATE);
            var last = pf.getLong("d", 0L);
            if (last.toInt() == 0) {
                pf.edit().putLong("d", DateTime().getMillis()).commit()
                //ads admob avalible
                if (interstitialAd.isLoaded) {
                    Log.d("AdMob", "Admob Interistial showed")
                    interstitialAd.show();
                }
                else {
                    interstitialAd.loadAd(request)
                    action_close()
                }

            } else if (Seconds.secondsBetween(DateTime(pf.getLong("d", 0L)), DateTime()).seconds >= 40) {
                pf.edit().putLong("d", DateTime().getMillis()).commit();
                    if (interstitialAd.isLoaded) {
                        Log.d("AdMob", "Admob Interistial showed")
                        interstitialAd.show();
                    } else {
                        interstitialAd.loadAd(request)
                        action_close()
                    }
            } else
                action_close()
        }

        fun loadBanner(banner: AdView, config: Boolean) {
            if (FirebaseRemoteConfig.getInstance().getBoolean("ads")) {
                val adRequest = AdRequest.Builder().build()
                banner.loadAd(adRequest)
                banner.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        banner.setVisibility(View.VISIBLE)
                    }

                    override fun onAdFailedToLoad(errorCode: Int) {
                        super.onAdFailedToLoad(errorCode)
                        //                    mAdView.setVisibility(View.GONE);
                    }
                }
            }
        }

        fun rate(ctx: Context){
            val ratingDialog = RatingDialog.Builder(ctx)
//                .threshold(3f)
                    .session(2)
                    .onRatingBarFormSumbit {
                        ctx.launchUrl("https://play.google.com/store/apps/details?id=${ctx.packageName}")
                    }.build()
            ratingDialog.show()
        }
    }
}
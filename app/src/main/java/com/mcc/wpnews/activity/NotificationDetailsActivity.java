package com.mcc.wpnews.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mcc.wpnews.R;
import com.mcc.wpnews.data.constant.AppConstant;
import com.mcc.wpnews.utility.ActivityUtils;

public class NotificationDetailsActivity extends BaseActivity {

    private Context mContext;
    private Activity mActivity;

    private TextView titleView, messageView;
    private Button linkButton;
    private String title, message, postId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = NotificationDetailsActivity.this;
        mContext = mActivity.getApplicationContext();
        initView();
        initVeritable();
        initFunctionality();
        initListeners();
    }

    private void initVeritable() {
        Bundle extras = getIntent().getExtras();
        title = extras.getString(AppConstant.BUNDLE_KEY_TITLE);
        message = extras.getString(AppConstant.BUNDLE_KEY_MESSAGE);
        postId = extras.getString(AppConstant.BUNDLE_KEY_POST_ID);
    }

    private void initView() {
        setContentView(R.layout.activity_notification_details);

        titleView = (TextView) findViewById(R.id.title);
        messageView = (TextView) findViewById(R.id.message);
        linkButton = (Button) findViewById(R.id.linkButton);

        initToolbar();
        setToolbarTitle(getString(R.string.notifications));
        enableBackButton();
    }


    private void initFunctionality() {

        titleView.setText(title);
        messageView.setText(message);

        if (postId != null && !postId.isEmpty()) {
            linkButton.setEnabled(true);
        } else {
            linkButton.setEnabled(false);
        }
    }

    private void initListeners() {
        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = -1;
                try {
                    id = Integer.parseInt(postId);
                } catch (Exception e) {
                }
                ActivityUtils.getInstance().invokePostDetails(mActivity, PostDetailsActivity.class, id, false);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

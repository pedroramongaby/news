package com.mcc.wpnews.utility;

import android.content.Context;
import android.content.pm.PackageManager;

public class UtilsWhatsApp {

    public static String getInstalledWhatsAppVersion(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.whatsapp", 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    public static Boolean isWhatsAppInstalled(Context context) {
        Boolean res;

        try {
            context.getPackageManager().getPackageInfo("com.whatsapp", 0);
            res = true;
        } catch (PackageManager.NameNotFoundException e) {
            res = false;
        }

        return res;
    }

//    public static Boolean isUpdateAvailable(String installedVersion, String latestVersion) {
//        Version installed = new Version(installedVersion);
//        Version latest = new Version(latestVersion);
//
//        return installed.compareTo(latest) < 0;
//    }

}

package com.mcc.wpnews.data.constant;

/**
 * Created by Ashiq on 4/13/2017.
 */

public class AppConstant {

    public static final String EMPTY = " ";

    public static final String BUNDLE_KEY_TITLE = "title";
    public static final String BUNDLE_KEY_MESSAGE = "message";
    public static final String BUNDLE_KEY_URL = "url";
    public static final String BUNDLE_FROM_PUSH = "from_push";
    public static final long SITE_CACHE_SIZE = 10 * 1024 * 1024;
    public static final String BUNDLE_KEY_POST_ID = "post_id";
    public static final String BUNDLE_KEY_POST = "post";
    public static final String BUNDLE_KEY_CATEGORY_ID = "category_id";
    public static final String BUNDLE_KEY_CATEGORY_LIST = "category_list";
    public static final String BUNDLE_KEY_SUB_CATEGORY_LIST = "sub_category_list";
    public static final String BUNDLE_KEY_MENU_ID = "post_id";
    public static final String BUNDLE_KEY_SUB_MENU = "sub_menu";
    public static final String BUNDLE_KEY_ALL_COMMENT = "all_comment";
    public static final String BUNDLE_KEY_ALL_ZERO_PARENT_COMMENT = "zero_parent_comments";
    public static final String BUNDLE_KEY_CLICKED_COMMENT = "clicked_comment";
    public static final String BUNDLE_KEY_DIALOG_FRAGMENT = "dialog_fragment";
    public static final String BUNDLE_KEY_SHOULD_DIALOG_OPEN = "should_dialog_open";
    public static final String BUNDLE_KEY_SEARCH_TEXT = "search_text";
    public static final int THIS_IS_COMMENT = -1;
    public static final int REQUEST_CODE_COMMENT = 0;
    public static final String BUNDLE_KEY_COMMENT_STATUS = "comment_success_status";
    public static final String NEW_NOTI = "new_notification";
    public static final String DOT = ".";

    public static final String ARG_CLICKED_POST_ID = "clicked_post_id";
    public static final String ARG_COMMENT_ID = "comment_id";
    public static final String COMMENT_MESSAGE = "message";
    public static final String COMMENT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final Double COMMENT_ID = 500.0;
    public static final Double COMMENT_PARENT_ID = 0.0;

    public static final int DEFAULT_PAGE = 1;


    // replace call number, sms number and email address by yours
    public static final String CALL_NUMBER = "911"; // replace by your support number

    public static final String SMS_NUMBER = "+123456789"; // replace by your support sms number
    public static final String SMS_TEXT = "Send your feedback to improve our service..."; // replace by your message

    public static final String EMAIL_ADDRESS = "updatesforwhatsapp@gmail.com"; // replace by your support sms number
    public static final String EMAIL_SUBJECT = "Feedback"; // replace by your message
    public static final String EMAIL_BODY = "Send your feedback to improve our service..."; // replace by your message


    // Menu item type
    public static final String MENU_ITEM_CATEGORY = "category";
    public static final String MENU_ITEM_PAGE = "page";
    public static final String MENU_ITEM_CUSTOM = "custom";
    public static final String MENU_ITEM_POST = "post";

    public static final String VIDEO_JSON="{\"items\":[\n" +
            "  {\n" +
            "    \"id\": \"1\",\n" +
            "    \"title\": \"How to Set Video Status on Whatsapp\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=A2TZHXobDHQ\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/A2TZHXobDHQ/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"2\",\n" +
            "    \"title\": \"How to Recover Deleted Whatsapp Messages\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=PIR2qTAPUVQ\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/PIR2qTAPUVQ/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"3\",\n" +
            "    \"title\": \"How to record WhatsApp Voice Call\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=XVuTDM2-1p8\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/XVuTDM2-1p8/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"4\",\n" +
            "    \"title\": \"How To Delete Whatsapp Contact\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=VJTUduwJnqQ\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/VJTUduwJnqQ/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"5\",\n" +
            "    \"title\": \"Typing Tricks\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=vggQvCWqiHY\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/vggQvCWqiHY/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"6\",\n" +
            "    \"title\": \"How to make international calls\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=PFF-NjT3NwE\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/PFF-NjT3NwE/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"7\",\n" +
            "    \"title\": \"How to Stop Sharing Your Live Location\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=eb9gULOuMgE\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/eb9gULOuMgE/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"8\",\n" +
            "    \"title\": \"How to Mention Someone in a Group Chat\",\n" +
            "    \"video\": \"https://www.youtube.com/watch?v=xv-X941Wq7s\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/xv-X941Wq7s/hqdefault.jpg\"\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": \"9\",\n" +
            "    \"appname_en\": \"How to Automatically Download Media Files\",\n" +
            "    \"appname_es\": \"https://www.youtube.com/watch?v=oUe1MhUhXfo\",\n" +
            "    \"image\": \"https://img.youtube.com/vi/oUe1MhUhXfo/hqdefault.jpg\"\n" +
            "  }\n" +
            "]\n" +
            "}";

}

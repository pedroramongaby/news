package com.mcc.wpnews.listeners;

import android.view.View;

/**
 * Created by Nasir on 8/1/2016.
 */
public interface OnItemClickListener {
    void onItemListener(View view, int position);
}

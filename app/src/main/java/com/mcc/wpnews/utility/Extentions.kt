package leoperez.com.myapplication.extention

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.InputFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.regex.Pattern


/**
 * Created by Leo Perez Ortíz on 6/1/18.
 */

fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

@GlideModule
class GalleryModule : AppGlideModule()// Intentionally empty.

inline fun Context.launchUrl(url: String){
    var intent= Intent(Intent.ACTION_VIEW,Uri.parse(url))
    startActivity(intent)
}

fun getEditTextFilter(): Array<out InputFilter>? {

    var array = Array<InputFilter>(1, init = {
        object : InputFilter {
            override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {

                var keepOriginal = true
                val sb = StringBuilder(end - start)
                for (i in start until end) {
                    val c = source[i]
                    if (isCharAllowed(c))
                    // put your condition here
                        sb.append(c)
                    else
                        keepOriginal = false
                }
                if (keepOriginal)
                    return null
                else {
                    if (source is Spanned) {
                        val sp = SpannableString(sb)
                        TextUtils.copySpansFrom(source, start, sb.length, null, sp, 0)
                        return sp
                    } else {
                        return sb
                    }
                }
            }

            private fun isCharAllowed(c: Char): Boolean {
                val ps = Pattern.compile("^[a-zA-Z ]+$")
                val ms = ps.matcher(c.toString())
                return ms.matches()
            }
        }
    })

    return array
}
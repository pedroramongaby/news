package com.mcc.wpnews.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.animation.AnimationUtils
import com.google.gson.Gson
import com.mcc.wpnews.R
import com.mcc.wpnews.adapters.VideoAdapter
import com.mcc.wpnews.data.constant.AppConstant
import com.mcc.wpnews.models.Item
import com.mcc.wpnews.models.Videos
import com.mcc.wpnews.utility.AdUtils
import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.content_video.*
import kotlinx.android.synthetic.main.topmain.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

class VideoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        AdUtils.getInstance(this).showBannerAd(adView)
        initVideos()
    }

    fun initVideos() {
        val videos= Gson().fromJson(AppConstant.VIDEO_JSON,Videos::class.java)
        rv.layoutAnimation= AnimationUtils.loadLayoutAnimation(this,R.anim.fall_layout_animator_home)
        launch {
            rv.adapter= VideoAdapter(loadData().await())
        }

    }

    fun loadData(): Deferred<ArrayList<Item>>{
      return  async(CommonPool){
            Gson().fromJson(AppConstant.VIDEO_JSON,Videos::class.java).items
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}

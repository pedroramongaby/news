package com.mcc.wpnews.app

import android.content.Intent
import android.os.Handler
import android.support.multidex.MultiDexApplication
import android.text.TextUtils
import android.util.Log
import com.blankj.utilcode.util.Utils
import com.facebook.FacebookSdk
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.mcc.wpnews.R
import com.mcc.wpnews.activity.UpdateActivity
import com.mcc.wpnews.utility.ExtraAdsManager
import com.onesignal.OneSignal
import leoperez.com.myapplication.extention.launchUrl
import org.jetbrains.anko.intentFor

/**
 * Created by Ashiq on 4/13/2017.
 */

class MyApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        FacebookSdk.sdkInitialize(getApplicationContext());
        OneSignal.startInit(this).setNotificationOpenedHandler { result ->
            val launchUri = result.notification.payload.launchURL
            val data = result.notification.payload.additionalData
            if (!TextUtils.isEmpty(launchUri)) {
                launchUrl(launchUri)
            } else if (data["hide"] != null) {
                startActivity(intentFor<UpdateActivity>()
                        .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            }
        }.init()
        Utils.init(this)
        ExtraAdsManager.initManager(this, getString(R.string.interstitial_ad_unit_id), getString(R.string.interstitial_facebookk))
        Handler().postDelayed({
            var remote = FirebaseRemoteConfig.getInstance()
            remote.setConfigSettings(FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build())
            remote.setDefaults(R.xml.remote)
            remote.fetch(0).addOnCompleteListener {
                if (it.isSuccessful) {
                    var act=remote.activateFetched()
                    Log.d("Fetch success", "${remote.getBoolean("ads")}")
                }
            }
        },1000)
    }
}
